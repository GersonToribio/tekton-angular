import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared';
import { TimelineStorePostComponent } from './timeline/components/store-post/store-post.component';
import { TimelineStoreCreatePostComponent } from './timeline/components/store-create-post/store-create-post.component';

@NgModule({
  declarations: [
    TimelineStorePostComponent,
    TimelineStoreCreatePostComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  providers: [
  ],
  exports: [
    TimelineStorePostComponent,
    TimelineStoreCreatePostComponent
  ]
})
export class StoreSharedModule { }
