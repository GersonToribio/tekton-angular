import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';

import { ActivatedRoute } from '@angular/router';

import { RegisterService } from 'src/app/services/register/register.service';
import { ErrorMatcherEmails } from 'src/app/shared/error-email/error-email.service';
import { MIN_PASSWORD_LENGTH } from './model';
import { isNotNull } from 'src/app/services/utils.service';

// Components
import { ModalRegisterComponent } from '../modal-register/modal-register.component';

@Component({
    selector: 'app-register-storeregister',
    templateUrl: './store-register.component.html',
    styleUrls: ['./styles/store-register.styles.scss'],
    encapsulation: ViewEncapsulation.None
})

export class RegisterStoreRegisterComponent implements OnInit, OnDestroy {
    // Subscription container to chain all Observer's subscriptions
    private subscriptionChain = new Subscription();

    // Contain forms tags
    private FORM_FIELD = {};
    // Contain all form to manage
    public  form: FormGroup;

    // Minimum length for passwords
    public min_password_length = MIN_PASSWORD_LENGTH;

    // Matcher emails
    matcher = new ErrorMatcherEmails();

    constructor(private route: ActivatedRoute,
                public dialog: MatDialog,
                private registerService: RegisterService,
                public fb: FormBuilder) {
    }

    ngOnInit() {
        // Initializing forms
        this.initializingForm();
        // Subscribe to register options
        this.subscribeMessageRegister();
    }

    private initializingForm() {
        this.FORM_FIELD = {
            user_name: new FormControl('', Validators.required),
            email: new FormControl('', Validators.compose([
                Validators.email,
                Validators.required
            ])),
            password: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(MIN_PASSWORD_LENGTH)
            ])),
            repeat_password: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(MIN_PASSWORD_LENGTH)
            ]))
        };
        this.form = this.fb.group(this.FORM_FIELD, { validator: [this.checkPasswords] });
    }

    validateInputs() {
        const pass = this.form.controls.password.value;
        const confirmPass = this.form.controls.repeat_password.value;

        console.log(pass, confirmPass);
        const value = (pass === confirmPass) ? false : true;
        return value;
    }

    private subscribeMessageRegister() {
        const obsHasMessage$ = this.registerService.hasMessage.subscribe((hasError) => {
            if (isNotNull(hasError)) {
                this.dialog.open(ModalRegisterComponent, {
                    width: '250px',
                    data: Object.assign({}, hasError, {email: this.email.value, password: this.password.value})
                });
            }
        });
        this.subscriptionChain.add(obsHasMessage$);
    }

    private checkPasswords(group: FormGroup) {
        const pass = group.controls.password.value;
        const confirmPass = group.controls.repeat_password.value;

        return pass === confirmPass ? null : { notSame: true };
    }

    // -- GETTERS
    get user_name() { return this.form.get('user_name'); }
    get email() { return this.form.get('email'); }
    get password() { return this.form.get('password'); }
    get repeat_password() { return this.form.get('repeat_password'); }

    createFormData() {
        // Create body of our ne payload
        const formData = {
            user_name: this.user_name.value,
            email: this.email.value,
            password: this.password.value,
            repeat_password: this.repeat_password.value
        };
        return formData;
    }

    onSubmit() {
        if (this.form.valid && !this.validateInputs()) {
            const message = this.registerService.registerNewUser(this.createFormData());
        }
    }

    ngOnDestroy() {
        // Unsubcribe from all subscription registered
        this.subscriptionChain.unsubscribe();
    }
}
