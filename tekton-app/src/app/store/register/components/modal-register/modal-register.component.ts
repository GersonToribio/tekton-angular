import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AuthService } from 'src/app/services/login/auth.service';
import { isNotNull } from 'src/app/services/utils.service';
import { Router } from '@angular/router';

export interface DialogData {
    message: string;
    success: boolean;
    email: string;
    password: string;
}

@Component({
    selector: 'app-modal-register',
    templateUrl: 'modal-register.component.html',
  })
  export class ModalRegisterComponent {
    constructor(
      public dialogRef: MatDialogRef<ModalRegisterComponent>,
      private authService: AuthService,
      private router: Router,
      @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

    routingToPlatform(): void {
        if (this.data.success) {
            // Need to do something when is correct
            this.authService.signin(this.data.email, this.data.password)
                .subscribe(res => {
                    if (isNotNull(res)) {
                        this.router.navigate(['/timeline']);
                        this.dialogRef.close();
                    } else {
                        console.log('Error');
                    }
                });
        } else {
            this.dialogRef.close();
        }
    }
}
