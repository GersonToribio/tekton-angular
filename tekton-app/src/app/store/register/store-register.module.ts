import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { StoreSharedModule } from '../';
import { SharedModule } from '../../shared';

import { RegisterStoreRegisterComponent } from './components/store-register/store-register.component';
import { ServicesModule } from 'src/app/services/services.module';
import { ModalRegisterComponent } from './components/modal-register/modal-register.component';

export const registerRoutes = [
    {
      path: '',
      component: RegisterStoreRegisterComponent,
      resolve: {
      }
    }
];

@NgModule({
    declarations: [
        RegisterStoreRegisterComponent,
        ModalRegisterComponent
    ],
    imports: [
      RouterModule.forChild(registerRoutes),
      CommonModule,
      SharedModule,
      StoreSharedModule,
      ServicesModule
    ],
    providers: [
    ],
    entryComponents: [
        ModalRegisterComponent
    ]
})
export class StoreRegisterModule { }
