import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

import { Subscription } from 'rxjs';

import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/login/auth.service';
import { isNotNull } from 'src/app/services/utils.service';

@Component({
    selector: 'app-login-storelogin',
    templateUrl: './store-login.component.html',
    styleUrls: ['./styles/store-login.styles.scss'],
    encapsulation: ViewEncapsulation.None
})

export class LoginStoreLoginComponent implements OnInit, OnDestroy {
    // Subscription container to chain all Observer's subscriptions
    private subscriptionChain = new Subscription();

    // Current password or email are incorrect
    public isIncorrect = false;

    // Contain forms tags
    private FORM_FIELD = {};
    // Contain all form to manage
    public  form: FormGroup;

    constructor(private route: ActivatedRoute,
        private authService: AuthService,
        private router: Router,
        public fb: FormBuilder) {
    }

    ngOnInit() {
        // Initializing forms
        this.initializingForm();
    }

    private initializingForm() {
        this.FORM_FIELD = {
            email: new FormControl('', Validators.compose([
                Validators.email,
                Validators.required
            ])),
            password: new FormControl('', Validators.compose([
                Validators.required
            ]))
        };
        this.form = this.fb.group(this.FORM_FIELD);
    }

    // -- GETTERS
    get email() { return this.form.get('email'); }
    get password() { return this.form.get('password'); }

    onSubmit() {
        if (this.form.valid) {
            this.authService.signin(this.email.value, this.password.value)
                .subscribe(res => {
                    if (isNotNull(res)) {
                        this.isIncorrect = false;
                        this.router.navigate(['/timeline']);
                    } else {
                        this.isIncorrect = true;
                    }
                });
        }
    }

    ngOnDestroy() {
        // Unsubcribe from all subscription registered
        this.subscriptionChain.unsubscribe();
    }
}
