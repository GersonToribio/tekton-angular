import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { StoreSharedModule } from '../';
import { SharedModule } from '../../shared';

import { ServicesModule } from 'src/app/services/services.module';

import { LoginStoreLoginComponent } from './components/store-login/store-login.component';

export const loginRoutes = [
    {
      path: '',
      component: LoginStoreLoginComponent,
      resolve: {
      }
    }
];

@NgModule({
    declarations: [
        LoginStoreLoginComponent,
    ],
    imports: [
      RouterModule.forChild(loginRoutes),
      CommonModule,
      SharedModule,
      StoreSharedModule,
      ServicesModule
    ],
    providers: [
    ],
})
export class StoreLoginModule { }
