export { ModalDeleteComponent } from './components/modal-delete/modal-delete.component';
export { TimelineStoreTimelineComponent } from './components/store-timeline/store-timeline.component';
export { TimelinePostResolver } from './resolvers/timeline-posts.resolver';

export { StoreTimelineModule } from './store-timeline.module';
