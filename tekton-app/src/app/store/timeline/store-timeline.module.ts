import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { StoreSharedModule } from '../';
import { SharedModule } from '../../shared';
import { AuthService } from 'src/app/services/login/auth.service';

import { ServicesModule } from 'src/app/services/services.module';
import { TimelineStoreTimelineComponent } from './components/store-timeline/store-timeline.component';
import { TimelinePostResolver } from './resolvers/timeline-posts.resolver';
import { ModalDeleteComponent } from './components/modal-delete/modal-delete.component';
import { CoreModule } from 'src/app/core/core.module';

export const timelineRoutes = [
    {
      path: '',
      component: TimelineStoreTimelineComponent,
      resolve: {
          data: TimelinePostResolver
      },
      canActivate: [ AuthService ]
    }
];

@NgModule({
    declarations: [
        TimelineStoreTimelineComponent,
        ModalDeleteComponent
    ],
    imports: [
      RouterModule.forChild(timelineRoutes),
      CommonModule,
      SharedModule,
      StoreSharedModule,
      ServicesModule,

      CoreModule,
    ],
    providers: [
        TimelinePostResolver
    ],
    entryComponents: [
        ModalDeleteComponent
    ]
})
export class StoreTimelineModule { }
