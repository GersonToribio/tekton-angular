import { Component, ViewEncapsulation, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

import { MatDialog } from '@angular/material';

import { Subscription } from 'rxjs';
import { TimelineService } from 'src/app/services/timeline/timeline.service';
import { AuthService } from 'src/app/services/login/auth.service';
import { isNotNull } from 'src/app/services/utils.service';

import * as moment from 'moment';
import 'moment/locale/pt-br';
import { ModalDeleteComponent } from '../modal-delete/modal-delete.component';


@Component({
    selector: 'app-timeline-storepost',
    templateUrl: './store-post.component.html',
    styleUrls: ['./styles/store-post.styles.scss'],
    encapsulation: ViewEncapsulation.None
})

export class TimelineStorePostComponent implements OnInit {
    @Input() item;

    // Contain forms tags
    private FORM_FIELD = {};
    // Contain all form to manage
    public  form: FormGroup;
    // Edit mode
    public editPostMode = false;
    // People how like the post
    public list_people_like = '';

    // Obtain the user logged
    public user_logged;
    // quantity of likes for the post
    public quantity_likes = 0;

    constructor(public fb: FormBuilder,
        public dialog: MatDialog,
        private timelineService: TimelineService,
        private authService: AuthService) {
        // Obtain user logged
        this.user_logged = this.authService.getUserLogged();
    }

    ngOnInit() {
        // Initializing forms
        this.initializingForm();
    }

    private initializingForm() {
        this.FORM_FIELD = {
            post: new FormControl(''),
        };
        this.form = this.fb.group(this.FORM_FIELD);

        // Set quantity of likes
        this.quantity_likes = this.item.people_like.length;
    }

    public getStyleButton(option) {
        if (option === 'LIKE') {
            let people_like;
            people_like = this.item.people_like;
            const liked =  people_like.find(i => i.email === this.user_logged.email);
            if (isNotNull(liked)) {
                return 'primary';
            }
            return '';
        } else if (option === 'DISLIKE') {
            let people_dislike;
            people_dislike = this.item.people_dislike;
            const liked =  people_dislike.find(i => i.email === this.user_logged.email);
            if (isNotNull(liked)) {
                return 'primary';
            }
            return '';
        }
    }

    public getColorLike(option) {
        if (option === 'LIKE') {
            let people_like;
            people_like = this.item.people_like;
            const liked =  people_like.find(i => i.email === this.user_logged.email);
            if (isNotNull(liked)) {
                return '';
            }
            return 'not-selected';
        } else if (option === 'DISLIKE') {
            let people_dislike;
            people_dislike = this.item.people_dislike;
            const liked =  people_dislike.find(i => i.email === this.user_logged.email);
            if (isNotNull(liked)) {
                return '';
            }
            return 'not-selected';
        }
    }

    public validateUser() {
        return this.user_logged.email === this.item.owner;
    }

    public wasEdited() {
        return this.item.flag_edited;
    }

    public showUserLike() {
        this.list_people_like = '';
        let cant = 0;
        for (const i of this.item.people_like) {
            if (cant < 4) {
                this.list_people_like += i.user_name + '\n';
            } else {
                this.list_people_like += 'More...';
                return this.list_people_like;
            }
            cant++;
        }
        return this.list_people_like;
    }

    // -- GETTERS
    get post() { return this.form.get('post'); }

    private getFormData() {
        const formData = {
            email: this.user_logged.email,
            id_post: this.item.id,
            user_name: this.user_logged.user_name
        };
        return formData;
    }

    public editPost() {
        this.editPostMode = true;
        // Inital value of textarea
        this.post.setValue(this.item.text);
    }

    public deletePost() {
        this.dialog.open(ModalDeleteComponent, {
            width: '250px',
            data: {id_post: this.item.id, message: 'Do you want to delete this post ?'}
        });
    }

    public noEditPost() {
        this.editPostMode = false;
        // reset value of textarea
        this.post.setValue('');
    }

    public changeBackground() {
        if (this.editPostMode) {
            return 'edit-post';
        }
        return '';
    }

    private getFormDataEdit() {
        const current_data = new Date();
        const formData = {
            text: this.post.value,
            updated_at: moment(current_data).format('DD/MM/YYYY h:mm:ss a'),
            id_post: this.item.id
        };
        return formData;
    }

    public actionEdit() {
        this.timelineService.editPost(this.getFormDataEdit());
    }

    // Incerement likes
    public actionLike() {
        this.timelineService.likePost(this.getFormData());
    }

    // Incerement dislikes
    public actionDislike() {
        this.timelineService.dislikePost(this.getFormData());
    }

}
