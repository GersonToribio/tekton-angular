import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { TimelineService } from 'src/app/services/timeline/timeline.service';

export interface DialogData {
    message: string;
    id_post: number;
}

@Component({
    selector: 'app-modal-delete',
    templateUrl: 'modal-delete.component.html',
  })
  export class ModalDeleteComponent {
    constructor(
      public dialogRef: MatDialogRef<ModalDeleteComponent>,
      private timelineService: TimelineService,
      @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

    deletePost() {
        this.timelineService.deletePost({id_post: this.data.id_post});
        this.dialogRef.close();
    }
}
