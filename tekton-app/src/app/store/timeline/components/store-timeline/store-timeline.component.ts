import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs';
import { TimelineService } from 'src/app/services/timeline/timeline.service';
import { isNotNull } from 'src/app/services/utils.service';

@Component({
    selector: 'app-timeline-storetimeline',
    templateUrl: './store-timeline.component.html',
    encapsulation: ViewEncapsulation.None
})

export class TimelineStoreTimelineComponent implements OnInit, OnDestroy {
    // Subscription container to chain all Observer's subscriptions
    private subscriptionChain = new Subscription();

    // All list with posts
    listPosts = [];

    constructor(private route: ActivatedRoute,
                private timelineService: TimelineService) {
        this.listPosts = route.snapshot.data['data'].listPosts;
        // console.log('this.listPosts', this.listPosts);
    }

    ngOnInit() {
        this.subscribeToDataTimeline();
    }

    private subscribeToDataTimeline() {
        const obsPost$ = this.timelineService.posts.subscribe(data => {
            if (isNotNull(data)) {
                // Observer all changes of our posts
                this.listPosts = data;
            }
        });
        this.subscriptionChain.add(obsPost$);
    }

    ngOnDestroy() {
        // Unsubcribe from all subscription registered
        this.subscriptionChain.unsubscribe();
    }
}
