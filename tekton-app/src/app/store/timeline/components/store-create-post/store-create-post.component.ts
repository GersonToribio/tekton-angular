import { Component, ViewEncapsulation, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

import { Subscription } from 'rxjs';
import { isNotNull } from 'src/app/services/utils.service';
import { TimelineService } from 'src/app/services/timeline/timeline.service';

import * as moment from 'moment';
import 'moment/locale/pt-br';

@Component({
    selector: 'app-timeline-storecreatepost',
    templateUrl: './store-create-post.component.html',
    styleUrls: ['./styles/store-create-post.styles.scss'],
    encapsulation: ViewEncapsulation.None
})

export class TimelineStoreCreatePostComponent implements OnInit {
    // Contain forms tags
    private FORM_FIELD = {};
    // Contain all form to manage
    public  form: FormGroup;

    constructor(public fb: FormBuilder,
                private timelineService: TimelineService) {
    }

    ngOnInit() {
        // Initializing forms
        this.initializingForm();
    }

    private initializingForm() {
        this.FORM_FIELD = {
            post: new FormControl(''),
        };
        this.form = this.fb.group(this.FORM_FIELD);
    }

    private getFormPost() {
        const current_data = new Date();
        const formData = {
            created_at: moment(current_data).format('DD/MM/YYYY h:mm:ss a'),
            updated_at: moment(current_data).format('DD/MM/YYYY h:mm:ss a'),
            flag_edited: false,
            text: this.post.value,
            people_like: [],
            people_dislike: [],
            visible: true
        };
        return formData;
    }

    private cleanTextArea() {
        this.post.setValue('');
    }

    public sharePost() {
        if (isNotNull(this.post.value)) {
            this.timelineService.savePost(this.getFormPost());
            this.cleanTextArea();
        }
    }

    // -- GETTERS
    get post() { return this.form.get('post'); }
}
