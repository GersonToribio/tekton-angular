import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { forkJoin } from 'rxjs';

import { TimelineService } from 'src/app/services/timeline/timeline.service';

@Injectable()
export class TimelinePostResolver implements Resolve<any> {
    constructor(
        private timelineService: TimelineService
      ) {}

    resolve() {
        return new Promise((resolve, reject) => {
            this.timelineService.getListPosts()
                .then(
                    (posts) => {
                        resolve({
                            listPosts: posts
                        });
                    }
                );
        });
    }
}
