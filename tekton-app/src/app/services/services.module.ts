import { NgModule } from '@angular/core';
import { SharedModule } from '../shared';

import { RegisterService } from './register/register.service';
import { AuthService } from './login/auth.service';
import { TimelineService } from './timeline/timeline.service';

@NgModule({
  declarations: [
    // Shared components
  ],
  imports: [
    SharedModule,
  ],
  exports: [
  ],
  providers: [
    RegisterService,
    AuthService,
    TimelineService
  ]
})
export class ServicesModule { }
