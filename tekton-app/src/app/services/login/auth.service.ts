import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Observable, BehaviorSubject, of } from 'rxjs';
import { isNotNull } from '../utils.service';

@Injectable()
export class AuthService implements CanActivate {
    loggedInSubject: BehaviorSubject<boolean>;

    userAuthenticated = isNotNull(JSON.parse(localStorage.getItem('current_user'))) ? true : false;

    constructor(private router: Router) {
        this.loggedInSubject = new BehaviorSubject<boolean>(this.isAuthenticated());
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        // Here you should check if your user is logged in then return true, else return false and redirect him to login page
        if (!this.isAuthenticated()) {
          this.router.navigate(['/login']);
        }
        return this.isAuthenticated();
    }

    isAuthenticated(): boolean {
        return this.userAuthenticated;
    }

    signin(email: string, password: string): Observable<any> {
        // In this part I'm simulating all backends validations

        // Backend validations
        const bd_users = JSON.parse(localStorage.getItem('users'));
        let signinObservable;

        if (isNotNull(bd_users)) {
            const user = this.getUserOfBD(email, password, bd_users);
            if (isNotNull(user)) {
                signinObservable = of(user);
                localStorage.setItem('current_user', JSON.stringify(user));
            } else {
                signinObservable = of(null);
                localStorage.setItem('current_user', JSON.stringify(null));
            }
        } else {
            signinObservable = of(null);
            localStorage.setItem('current_user', JSON.stringify(null));
        }

        signinObservable.subscribe(
            res => {
              this.userAuthenticated = res;
              this.loggedInSubject.next(res);
            }
        );
        return signinObservable;
    }

    getUserLogged() {
        return JSON.parse(localStorage.getItem('current_user'));
    }

    logout() {
        // Set in BD the current_user to null
        this.userAuthenticated = null;
        localStorage.setItem('current_user', null);
    }

    // External BD functions
    private getUserOfBD = (email, password, bd_users: []) => {
        if (isNotNull(bd_users.find((i: any) => i.email === email && i.password === password))) {
            return bd_users.filter((i: any) => i.email === email && i.password === password)[0];
        }
        return null;
    }
}
