import { Injectable } from '@angular/core';

import { Observable, BehaviorSubject, of } from 'rxjs';
import { isNotNull } from '../utils.service';

@Injectable()
export class RegisterService {
    private _isLoading: BehaviorSubject<boolean>;
    private _hasMessage: BehaviorSubject<any>;
    private dataStore: {  // This is where we will store our data in memory
        isLoading: false,
        hasMessage: null
    };

    constructor(
    ) {
        this.dataStore = { isLoading: false, hasMessage: null };
        this._isLoading = new BehaviorSubject<boolean>(false);
        this._hasMessage = new BehaviorSubject<any>(null);
    }

    get isLoading() {
        return this._isLoading.asObservable();
    }

    get hasMessage() {
        return this._hasMessage.asObservable();
    }

    private setLoading(state) {
        this.dataStore.isLoading = state;
        this._isLoading.next(Object.assign({}, this.dataStore).isLoading);
    }

    private sethasMessage(message) {
        this.dataStore.hasMessage = message;
        this._hasMessage.next(Object.assign({}, this.dataStore).hasMessage);
    }

    public registerNewUser = ( payload ) => {
        this.setLoading(true);
        // In this part I'm simulating all backends validations

        // Backend validations
        const bd_users = JSON.parse(localStorage.getItem('users'));
        let message = '';

        if (isNotNull(bd_users)) {
            // Validate is user in BD
            if (this.isUserInBD(payload.email, bd_users)) {
                message = 'This user has been registered before';
                this.sethasMessage({message: message, success: false});
            } else {
                // Save new in 'database'
                bd_users.push(payload);
                localStorage.setItem('users', JSON.stringify( bd_users ));
                message = 'Register succeeded';
                this.sethasMessage({message: message, success: true});
            }
        } else {
            // Save new in 'database'
            localStorage.setItem('users', JSON.stringify( [payload] ));
            message = 'Register succeeded';
            this.sethasMessage({message: message, success: true});
        }

        this.setLoading(false);
        return message;
    }

    // External BD functions
    private isUserInBD = (email, bd_users: []) => {
        if (isNotNull(bd_users.find((i: any) => i.email === email))) {
            return true;
        }
        return false;
    }
}
