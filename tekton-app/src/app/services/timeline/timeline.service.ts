import { Injectable } from '@angular/core';

import { Observable, BehaviorSubject, of } from 'rxjs';
import { isNotNull } from '../utils.service';

@Injectable()
export class TimelineService {
    private _isLoading: BehaviorSubject<boolean>;
    private _posts: BehaviorSubject<any>;
    private dataStore: {  // This is where we will store our data in memory
        isLoading: false,
        posts: []
    };

    constructor(
        ) {
            this.dataStore = { isLoading: false, posts: [] };
            this._isLoading = new BehaviorSubject<boolean>(false);
            this._posts = new BehaviorSubject<any>(null);
        }

    get isLoading() {
        return this._isLoading.asObservable();
    }

    get posts() {
        return this._posts.asObservable();
    }

    private setLoading(state) {
        this.dataStore.isLoading = state;
        this._isLoading.next(Object.assign({}, this.dataStore).isLoading);
    }

    private setPosts(posts) {
        this.dataStore.posts = this.filterVisibleProject(posts);
        this._posts.next(Object.assign({}, this.dataStore).posts);
    }

    public getListPosts = (): Promise<any> => {
        return new Promise((resolve, reject) => {
            let listpostsObservable: Observable<any>;

            // In this part I'm simulating all backends validations

            // Backend validations
            const bd_posts = JSON.parse(localStorage.getItem('posts'));

            if (isNotNull(bd_posts)) {
                listpostsObservable = of(this.filterVisibleProject(bd_posts));
            } else {
                listpostsObservable = of([]);
            }

            listpostsObservable.subscribe(
                data => resolve(data),
                err => reject()
            );
        });
    }

    private filterVisibleProject(list) {
        return list.filter(i => i.visible === true);
    }

    public savePost = (payload) => {
        const current_user = JSON.parse(localStorage.getItem('current_user'));
        // Backend validations
        let bd_posts = JSON.parse(localStorage.getItem('posts'));

        // Vaidate if the user has been logged
        if (isNotNull(current_user) && isNotNull(bd_posts)) {
            const id = bd_posts.length + 1;
            const new_post = Object.assign({}, payload, {id: id, owner: current_user.email, owner_username: current_user.user_name});

            bd_posts.push(new_post);

            // Save in 'BD'
            localStorage.setItem('posts', JSON.stringify(bd_posts));

            // Update posts
            this.setPosts(bd_posts);
        } else if (isNotNull(current_user) ) {
            const id = 1;
            const new_post = Object.assign({}, payload, {id: id, owner: current_user.email, owner_username: current_user.user_name});

            bd_posts = [new_post];

            // Save in 'BD'
            localStorage.setItem('posts', JSON.stringify(bd_posts));

            // Update posts
            this.setPosts(bd_posts);
        }
    }

    public likePost = (payload) => {
        const current_user = JSON.parse(localStorage.getItem('current_user'));
        // Backend validations
        const bd_posts = JSON.parse(localStorage.getItem('posts'));

        // Vaidate if the user has been logged
        if (isNotNull(current_user) && isNotNull(bd_posts)) {
            let post = bd_posts.find(i => i.id === payload.id_post);
            const index = bd_posts.findIndex(i => i.id === payload.id_post);

            // Find the post in 'BD'
            if (isNotNull(post)) {
                // Verifiy if the user liked this post before
                const isUserInArrayLike = post.people_like.find(i => i.email === payload.email);
                const indexUserInArrayLike = post.people_like.findIndex(i => i.email === payload.email);

                const isUserInArrayDislike = post.people_dislike.find(i => i.email === payload.email);
                const indexUserInArrayDislike = post.people_dislike.findIndex(i => i.email === payload.email);

                let newArrayLikes = [];
                let newArrayDislikes = [];
                if (!isNotNull(isUserInArrayLike)) {
                    // New Array likes
                    post.people_like.push({user_name: payload.user_name, email: payload.email});
                    newArrayLikes = post.people_like;
                } else {
                    // New Array likes
                    post.people_like.splice(indexUserInArrayLike, 1);
                    newArrayLikes = post.people_like;
                }

                // The user dislike the post before
                if (isNotNull(isUserInArrayDislike)) {
                    post.people_dislike.splice(indexUserInArrayDislike, 1);
                    newArrayDislikes = post.people_dislike;
                } else {
                    newArrayDislikes = post.people_dislike;
                }

                post = {
                    ...post,
                    people_dislike: newArrayDislikes,
                    people_like: newArrayLikes
                };

                bd_posts[index] = post;

                // Save in 'BD'
                localStorage.setItem('posts', JSON.stringify(bd_posts));
                // Update posts
                this.setPosts(bd_posts);
            }
        }
    }


    public dislikePost = (payload) => {
        const current_user = JSON.parse(localStorage.getItem('current_user'));
        // Backend validations
        const bd_posts = JSON.parse(localStorage.getItem('posts'));

        // Vaidate if the user has been logged
        if (isNotNull(current_user) && isNotNull(bd_posts)) {
            let post = bd_posts.find(i => i.id === payload.id_post);
            const index = bd_posts.findIndex(i => i.id === payload.id_post);

            // Find the post in 'BD'
            if (isNotNull(post)) {
                // Verifiy if the user liked this post before
                const isUserInArrayLike = post.people_like.find(i => i.email === payload.email);
                const indexUserInArrayLike = post.people_like.findIndex(i => i.email === payload.email);

                const isUserInArrayDislike = post.people_dislike.find(i => i.email === payload.email);
                const indexUserInArrayDislike = post.people_dislike.findIndex(i => i.email === payload.email);

                let newArrayLikes = [];
                let newArrayDislikes = [];
                if (!isNotNull(isUserInArrayDislike)) {
                    // New Array dislikes
                    post.people_dislike.push({user_name: payload.user_name, email: payload.email});
                    newArrayDislikes = post.people_dislike;
                } else {
                    // New Array dislikes
                    post.people_dislike.splice(indexUserInArrayDislike, 1);
                    newArrayDislikes = post.people_dislike;
                }

                // The user like the post before
                if (isNotNull(isUserInArrayLike)) {
                    post.people_like.splice(indexUserInArrayLike, 1);
                    newArrayLikes = post.people_like;
                } else {
                    newArrayLikes = post.people_like;
                }

                post = {
                    ...post,
                    people_dislike: newArrayDislikes,
                    people_like: newArrayLikes
                };

                bd_posts[index] = post;

                // Save in 'BD'
                localStorage.setItem('posts', JSON.stringify(bd_posts));
                // Update posts
                this.setPosts(bd_posts);
            }
        }
    }

    public editPost = (payload) => {
        const current_user = JSON.parse(localStorage.getItem('current_user'));
        // Backend validations
        const bd_posts = JSON.parse(localStorage.getItem('posts'));

        // Vaidate if the user has been logged
        if (isNotNull(current_user) && isNotNull(bd_posts)) {
            const post = bd_posts.find(i => i.id === payload.id_post);
            const index = bd_posts.findIndex(i => i.id === payload.id_post);

            // Find the post in 'BD'
            if (isNotNull(post)) {
                post.text = payload.text;
                post.updated_at = payload.updated_at;
                post.flag_edited = true;

                bd_posts[index] = post;
                // Save in 'BD'
                localStorage.setItem('posts', JSON.stringify(bd_posts));
                // Update posts
                this.setPosts(bd_posts);
            }
        }
    }

    public deletePost = (payload) => {
        const current_user = JSON.parse(localStorage.getItem('current_user'));
        // Backend validations
        const bd_posts = JSON.parse(localStorage.getItem('posts'));

        // Vaidate if the user has been logged
        if (isNotNull(current_user) && isNotNull(bd_posts)) {
            const post = bd_posts.find(i => i.id === payload.id_post);
            const index = bd_posts.findIndex(i => i.id === payload.id_post);

            // Find the post in 'BD'
            if (isNotNull(post)) {
                post.visible = false;

                bd_posts[index] = post;
                // Save in 'BD'
                localStorage.setItem('posts', JSON.stringify(bd_posts));
                // Update posts
                this.setPosts(bd_posts);
            }
        }
    }
}
