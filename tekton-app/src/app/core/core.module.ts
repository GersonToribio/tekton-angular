import { NgModule } from '@angular/core';

// Core components
import { TopNavbarComponent } from './top-navbar/top-navbar.component';

// Required modules
import { SharedModule } from '../shared';


@NgModule({
  declarations: [
    TopNavbarComponent,
  ],
  imports: [
    SharedModule,
  ],
  providers: [
  ],
  exports: [
    TopNavbarComponent,
  ]
})
export class CoreModule { }
