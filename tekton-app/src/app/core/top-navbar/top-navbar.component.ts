import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/login/auth.service';
import { isNotNull } from 'src/app/services/utils.service';

@Component({
  selector: 'app-top-navbar',
  styleUrls: ['./styles/top-navbar.styles.scss'],
  templateUrl: './top-navbar.component.html',
  encapsulation: ViewEncapsulation.None
})
export class TopNavbarComponent {
  navbarCollapsed = true;
  loggedInObservable: Observable<boolean>;

  // user logged
  public user_logged;

  constructor(public router: Router,
              private authService: AuthService) {
      // Obtain user logged
      this.user_logged = this.authService.getUserLogged();
  }

  TisNotNull(opcion) {
      return isNotNull(opcion);
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
