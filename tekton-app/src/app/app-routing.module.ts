import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  // STORE
  {
    path: 'register',
    loadChildren: './store/register/store-register.module#StoreRegisterModule'
  },
  {
    path: '',
    loadChildren: './store/login/store-login.module#StoreLoginModule'
  },
  {
    path: 'login',
    loadChildren: './store/login/store-login.module#StoreLoginModule'
  },
  {
    path: 'timeline',
    loadChildren: './store/timeline/store-timeline.module#StoreTimelineModule'
  },
  {
    path: '**',
    loadChildren: './store/login/store-login.module#StoreLoginModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
